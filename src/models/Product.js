const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Product = sequelize.define('Product', {
    price: {
        type: DataTypes.FLOAT,
        allowNull: false
    },

    titlename: {
        type: DataTypes.STRING,
        allowNull: false
    },
    quantity: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    description: {
        type: DataTypes.STRING,
    },
    banner_photos: {
        type: DataTypes.STRING,
        allowNull: false
    }
});

Product.associate = function(models) {
    Product.belongsTo(models.User);
}

module.exports = Product;
