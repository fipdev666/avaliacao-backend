const { response } = require('express');
const User = require('../models/User');
const Product = require('../models/Product')

const create = async(req, res) => {
    try{
        const product = await Product.create(req.body);
        return res.status(201).json({message: `${product.titlename} cadastrado(a) com sucesso`});
    } catch (err) {
        return res.status(500).json({error:err});
    }
};

const index = async(req,res) => {
    try {
        const products = await Product.findAll();
        return res.status(200).json({products});
    }catch(err){
        return res.status(500).json({err});
    }
};


const show = async(req,res) => {
    const {id} = req.params;
    try {
        const product = await Product.findByPk(id);
        return res.status(200).json({product});
    }catch(err){
        return res.status(500).json({err});
    }
};
const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await Product.update(req.body, {where: {id: id}});
        if(updated) {
            const product = await Product.findByPk(id);
            return res.status(200).send(product);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json("Produto não encontrado");
    }
};

const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const product = await Product.findByPk(id);
        const deleted = await Product.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json(`${product.titlename} removido(a) com sucesso`);
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("Produto não encontrado");
    }
};

const purchase = async(req,res) => {
    const {productId, userId} = req.params;
    try {
        const product = await Product.findByPk(productId);
        const user = await User.findByPk(userId);
        await product.setUser(user);
        return res.status(200).json({msg: `${user.name} comprou ${product.titlename}`});
        
    } catch (error) {
        return res.status(500).json({error});
    }
}

const cancelPurchase = async(req,res) => {
    const {id} = req.params;
    try {
        const product = await Product.findByPk(id);
        const user = await User.findByPk(product.dataValues.UserId);
        await product.setUser(null);
        return res.status(200).json({msg:`${user.name} cancelou a compra de ${product.titlename}`});
    } catch (error) {
        return res.status(500).json({error});
        
    }
}
module.exports = {
    index,
    show,
    create,
    update,
    destroy,
    purchase,
    cancelPurchase
};

